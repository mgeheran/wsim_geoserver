# Copyright (c) 2018 ISciences, LLC.
# All rights reserved.
#
# WSIM is licensed under the Apache License, Version 2.0 (the "License").
# You may not use this file except in compliance with the License. You may
# obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
import timeit
import urllib.request
import xml.etree.ElementTree

GS_URL_ROOT = None  # global variable to avoid passing this around everywhere
ERROR_HANDLER = None

def send_content(url, data, content_type, method='POST'):
    """
    Send data to a URL. Print the URL and response time to stdout.
    :param url: destination url
    :param data: content to send
    :param content_type: content-type for header, e.g., 'application/json' or 'text/xml'
    :param method: HTTP verb to use
    """
    if type(data) is str:
        data = data.encode('utf8')

    url = GS_URL_ROOT + '/' + url
    print(method, url)

    req = urllib.request.Request(url, data=data, method=method)
    if content_type:
        req.add_header('Content-Type', content_type)

    while True:
        try:
            start = timeit.default_timer()
            with urllib.request.urlopen(req, timeout=240) as f:
                end = timeit.default_timer()
                print(f.code, 'in', int(1000*(end - start)), 'ms')
                return f.read()
        except urllib.error.URLError as e:
            if not isinstance(e.reason, ConnectionRefusedError):
                end = timeit.default_timer()
                print(e.code, 'in', int(1000*(end - start)), 'ms', file=sys.stderr)
                if ERROR_HANDLER:
                    ERROR_HANDLER(url, e)
                    break
                else:
                    raise e
            print("Connection refused. Will retry in 1 second.")
            time.sleep(1)


def send_xml(url, data, method='POST'):
    """
    Convenience function to send XML data to a URL
    :param url: destination url
    :param data: content to send: text or ElementTree node
    :param method: HTTP verb to use
    """
    if type(data) is xml.etree.ElementTree.Element:
        data = xml.etree.ElementTree.tostring(data)

    return send_content(url, data, content_type='text/xml', method=method)


def send_zip(url, data, method='POST'):
    """
    Convenience function to send ZIP data to a URL
    :param url: destination url
    :param data: content to send (bytes)
    :param method: HTTP verb to use
    """
    return send_content(url, data, content_type='application/zip', method=method)


def configure(url, user, password, error_handler=None):
    """
    Globally configure urllib to use the provided user/password for basic auth
    when connection to url
    :param url: GeoServer REST API root url
    :param user: GeoServer admin user
    :param password: GeoServer admin password
    :param error_handler: A function that will be called with a url and error object,
                          in case of an error in a HTTP request, or a non-success HTTP
                          return code. If an error handler is not provided, exceptions
                          will be thrown in these cases.
    """
    global GS_URL_ROOT
    global ERROR_HANDLER
    GS_URL_ROOT = url

    if error_handler:
        ERROR_HANDLER = error_handler

    password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None, url, user, password)
    handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
    opener = urllib.request.build_opener(handler)
    urllib.request.install_opener(opener)
