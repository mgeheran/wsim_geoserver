#!/usr/bin/env bash

# Initializes wsim_geoserver with the path to the directory containing wsim
# output.

sudo docker exec \
  -it \
  wsim_geoserver \
  configure_geoserver.py \
    wsim_data init
