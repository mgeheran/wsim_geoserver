#!/usr/bin/env bash

# Starts the wsim_geoserver docker container.

sudo docker run \
  -d \
  --name wsim_geoserver \
  --publish 8080:8080 \
  --log-driver json-file \
  --log-opt max-size=100m \
  -v /data/runs:/opt/wsim_data:ro \
  isciences/wsim_geoserver:latest
