# GeoServer configuration for WSIM

This repository provides the following:

* A Docker container including GeoServer, extensions required for serving WSIM data, and a private PostGIS database for storing raster catalog information and a limited amount of Natural Earth data.
* A script that adds WSIM layers to GeoServer using the REST API

This configuration relies on the ImageMosaic extension to GeoServer, which is not without its quirks.
It requires that at least one of the netCDF files forming an ImageMosaic to be present at the time that coverages are created from the ImageMosaic.
Because the netCDF files are only attached to the Docker container at runtime, this means that the layer configuration cannot be baked into the container.

## Usage

To run GeoServer, first instantiate the Docker container and link `/opt/wsim_data` within the container to a local directory of WSIM workspaces, e.g.:

    docker run \
      -d \
      --name wsim_geoserver \
      --publish 8080:8080 \
      -v ~/wsim/runs:/opt/wsim_data:ro \ 
      isciences/wsim_geoserver:latest 

GeoServer will not write any data to the WSIM workspace, so it can be connected in read-only mode.

Next, tell the `configure_geoserver.py` script to initialize the layers:

    ./configure_geoserver.py --url http://localhost:8080/geoserver/rest production init

In this example, `production` indicates that a  GeoServer [workspace](http://docs.geoserver.org/stable/en/user/data/webadmin/workspaces.html) should be created with a name corresponding to the `production` subdirectory of `/opt/wsim_data`. (Directory and workspace names must be equivalent.) 

After all layers can be created, WSIM data can be requested through the [WMS](http://docs.geoserver.org/stable/en/user/services/wms/reference.html) capabilities of GeoServer.

If additional netCDF files ("granules", in GeoServer terminology) are added after layer creation, they can be made visible to GeoServer with the following command:

    ./configure_geoserver.py --url http://localhost:8080/geoserver/rest production harvest

