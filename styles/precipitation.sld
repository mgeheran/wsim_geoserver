<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor
  xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld"
  xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc"
  version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>precipitation</sld:Name>
    <sld:UserStyle>
      <sld:Name>precipitation</sld:Name>
      <sld:Title>Style for precipitation (mm)</sld:Title>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#FFFFFF" opacity="1" quantity="0"   label="0"      />
              <sld:ColorMapEntry color="#CDEBC6" opacity="1" quantity="${50*env('months')}"  label="50 mm/month"  />
              <sld:ColorMapEntry color="#78CAC5" opacity="1" quantity="${100*env('months')}" label="100 mm/month" />
              <sld:ColorMapEntry color="#3394C3" opacity="1" quantity="${150*env('months')}" label="150 mm/month" />
              <sld:ColorMapEntry color="#084485" opacity="1" quantity="${200*env('months')}" label="200 mm/month" />
            </sld:ColorMap>
            <sld:ContrastEnhancement/>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>
