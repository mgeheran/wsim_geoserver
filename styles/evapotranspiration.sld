<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor
  xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld"
  xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc"
  version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>evapotranspiration</sld:Name>
    <sld:UserStyle>
      <sld:Name>evapotranspiration</sld:Name>
      <sld:Title>Style for evapotranspiration (mm)</sld:Title>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#ffffe5" opacity="0" quantity="0"/>
              <sld:ColorMapEntry color="#ffffe5" opacity="1" quantity="0"   label="0"  />
              <sld:ColorMapEntry color="#f7fcb9" opacity="1" quantity="${25*env('months')}"  label="25 mm/month" />
              <sld:ColorMapEntry color="#d9f0a3" opacity="1" quantity="${50*env('months')}"  label="50 mm/month" />
              <sld:ColorMapEntry color="#addd8e" opacity="1" quantity="${100*env('months')}" label="100 mm/month"/>
              <sld:ColorMapEntry color="#78c679" opacity="1" quantity="${150*env('months')}" label="150 mm/month"/>
              <sld:ColorMapEntry color="#41ab5d" opacity="1" quantity="${200*env('months')}" label="200 mm/month"/>
              <sld:ColorMapEntry color="#238443" opacity="1" quantity="${250*env('months')}" label="250 mm/month"/>
              <sld:ColorMapEntry color="#006837" opacity="1" quantity="${300*env('months')}" label="300 mm/month"/>
              <sld:ColorMapEntry color="#004529" opacity="1" quantity="${400*env('months')}" label="400 mm/month"/>
            </sld:ColorMap>
            <sld:ContrastEnhancement/>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>
