<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>soil_moisture_change</sld:Name>
    <sld:UserStyle>
      <sld:Name>soil_moisture_change</sld:Name>
      <sld:Title>Style for soil moisture change (mm)</sld:Title>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ColorMap type="ramp">
              <sld:ColorMapEntry color="#9e0142" opacity="0" quantity="-400" />
              <sld:ColorMapEntry color="#9e0142" opacity="1" quantity="-400" label="-400 mm"/>
              <sld:ColorMapEntry color="#d53e4f" opacity="1" quantity="-100" label="-100 mm"/>
              <sld:ColorMapEntry color="#f46d43" opacity="1" quantity="-75"  />
              <sld:ColorMapEntry color="#fdae61" opacity="1" quantity="-50"  label="-50 mm"/>
              <sld:ColorMapEntry color="#fee08b" opacity="1" quantity="-25"  />
              <sld:ColorMapEntry color="#ffffbf" opacity="1" quantity="0"    label="0"/>
              <sld:ColorMapEntry color="#e6f598" opacity="1" quantity="25"   />
              <sld:ColorMapEntry color="#abdda4" opacity="1" quantity="50"   label="50 mm"/>
              <sld:ColorMapEntry color="#66c2a5" opacity="1" quantity="75"   />
              <sld:ColorMapEntry color="#3288bd" opacity="1" quantity="100"  label="100 mm"/>
              <sld:ColorMapEntry color="#5e4fa2" opacity="1" quantity="400"  label="400 mm"/>
            </sld:ColorMap>
            <sld:ContrastEnhancement/>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>
