<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor
  xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld"
  xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc"
  version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>temperature</sld:Name>
    <sld:UserStyle>
      <sld:Name>temperature</sld:Name>
      <sld:Title>Style for temperature (degrees C)</sld:Title>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#04366E" opacity="0" quantity="-100"/>
              <sld:ColorMapEntry color="#04366E" opacity="1" quantity="-100"/>
              <sld:ColorMapEntry color="#04366E" opacity="1" quantity="-20"  label="-20 C" />
              <sld:ColorMapEntry color="#1F5C9A" opacity="1" quantity="-10"  label="-10 C" />
              <sld:ColorMapEntry color="#0C9DD8" opacity="1" quantity="0"    label="0   C"/>
              <sld:ColorMapEntry color="#87C1E2" opacity="1" quantity="10"   label="10 C"/>
              <sld:ColorMapEntry color="#FED186" opacity="1" quantity="20"   label="20 C"/>
              <sld:ColorMapEntry color="#F46F53" opacity="1" quantity="25"   label="25 C"/>
              <sld:ColorMapEntry color="#C02441" opacity="1" quantity="30"   label="30 C"/>        
              <sld:ColorMapEntry color="#6D042F" opacity="1" quantity="40"   label="40 C"/>
            </sld:ColorMap>
            <sld:ContrastEnhancement/>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>
