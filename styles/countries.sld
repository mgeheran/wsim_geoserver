<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>Default Styler</sld:Name>
    <sld:UserStyle>
      <sld:Name>Default Styler</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <!-- Use this rule to mask out countries that are not part of the
               list specified in the "country" var. If the "country" var is
               not specified or is equal to "all", this rule will not fire.
               -->
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:Function name="strIndexOf">
                <ogc:Function name="env">
                  <ogc:Literal>country</ogc:Literal>
                  <ogc:PropertyName>name</ogc:PropertyName>
                </ogc:Function>       
                <ogc:PropertyName>name</ogc:PropertyName>
              </ogc:Function>
              <ogc:Literal>-1</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <sld:PolygonSymbolizer>
            <sld:Fill>
              <sld:CssParameter name="fill">#ffffff</sld:CssParameter>
              <sld:CssParameter name="fill-opacity">0.85</sld:CssParameter>
            </sld:Fill>
            <sld:Stroke>
              <sld:CssParameter name="stroke-opacity">0.5</sld:CssParameter>
              <sld:CssParameter name="stroke-width">0.1</sld:CssParameter>
              <sld:CssParameter name="stroke">#bebebe</sld:CssParameter>
            </sld:Stroke>
          </sld:PolygonSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <ogc:Filter>
            <!-- Draw country borders only (no fill) for countries listed in the
                 "country" var
                 -->
            <ogc:PropertyIsNotEqualTo>
              <ogc:Function name="strIndexOf">
                <ogc:Function name="env">
                  <ogc:Literal>country</ogc:Literal>
                  <ogc:PropertyName>name</ogc:PropertyName>
                </ogc:Function>   
                <ogc:PropertyName>name</ogc:PropertyName>
              </ogc:Function>
              <ogc:Literal>-1</ogc:Literal>
            </ogc:PropertyIsNotEqualTo>
          </ogc:Filter>
          <sld:PolygonSymbolizer>
            <sld:Stroke>
              <sld:CssParameter name="stroke-opacity">0.5</sld:CssParameter>
              <sld:CssParameter name="stroke-width">0.6</sld:CssParameter>
              <sld:CssParameter name="stroke">#000000</sld:CssParameter>
            </sld:Stroke>
          </sld:PolygonSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>
