<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name>return_period</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1.0</Opacity>
            <ColorMap type="ramp">
              <ColorMapEntry color="#9B0039" quantity="-60"  opacity="0"/>
              <ColorMapEntry color="#9B0039" quantity="-60"  opacity="1"/>
              <ColorMapEntry color="#9B0039" quantity="-40"  label="40" opacity="1"/>
              <ColorMapEntry color="#D44135" quantity="-20"  label="20" opacity="1"/>
              <ColorMapEntry color="#FF8D43" quantity="-10"  label="10" opacity="1"/>
              <ColorMapEntry color="#FFC754" quantity="-5"   label="5"  opacity="1"/>
              <ColorMapEntry color="#FFEDA3" quantity="-3"   label="3"  opacity="1"/>
              <ColorMapEntry color="#FFFFFF" quantity="-3"   opacity="0"/>
              <ColorMapEntry color="#FFFFFF" quantity="3"   opacity="0"/>
              <ColorMapEntry color="#CEFFAD" quantity="3"   label="5"  opacity="1"/>
              <ColorMapEntry color="#CEFFAD" quantity="5"   label="5"  opacity="1"/>
              <ColorMapEntry color="#00F3B5" quantity="10"  label="10" opacity="1"/>
              <ColorMapEntry color="#00CFCE" quantity="20"  label="20" opacity="1"/>
              <ColorMapEntry color="#009ADE" quantity="40"  label="40" opacity="1"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
