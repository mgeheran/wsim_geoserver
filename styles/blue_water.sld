<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor
  xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld"
  xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc"
  version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>blue_water</sld:Name>
    <sld:UserStyle>
      <sld:Name>blue_water</sld:Name>
      <sld:Title>Style for total blue water (mm)</sld:Title>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#f7fbff" opacity="0" quantity="0"/>
              <sld:ColorMapEntry color="#f7fbff" opacity="1" quantity="0"/>
              <sld:ColorMapEntry color="#deebf7" opacity="1" quantity="${1e1*env('months')}" label="10^1 mm/month"/>
              <sld:ColorMapEntry color="#c6dbef" opacity="1" quantity="${1e2*env('months')}" label="10^2 mm/month"/>
              <sld:ColorMapEntry color="#9ecae1" opacity="1" quantity="${1e3*env('months')}" label="10^3 mm/month"/>
              <sld:ColorMapEntry color="#6baed6" opacity="1" quantity="${1e4*env('months')}" label="10^4 mm/month"/>
              <sld:ColorMapEntry color="#4292c6" opacity="1" quantity="${1e5*env('months')}" label="10^5 mm/month"/>
              <sld:ColorMapEntry color="#2171b5" opacity="1" quantity="${1e6*env('months')}" label="10^6 mm/month"/>
              <sld:ColorMapEntry color="#08519c" opacity="1" quantity="${1e7*env('months')}" label="10^7 mm/month"/>
              <sld:ColorMapEntry color="#08306b" opacity="1" quantity="${1e8*env('months')}" label="10^8 mm/month"/>
            </sld:ColorMap>
            <sld:ContrastEnhancement/>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>

